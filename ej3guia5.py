filename = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():
    archivo = open(filename)
    dict = {}

    for counter, linea in enumerate(archivo):
        if counter != 0:
            archivo_linea = linea.split(",")
            pais = archivo_linea[PAIS].strip()
            codigo = archivo_linea[CODIGO].strip()
            anio = archivo_linea[ANIO].strip()
            co2 = archivo_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                            anio: co2}

            if dict.get(pais):
                dict[pais].update(temp_dic)
            else:
                dict[pais] = temp_dic
    archivo.close()

    return dict

def calcula_promedio_total(data):
    suma = 0
    total = total_paises(data)
    for key, value in data.items():
        pais = data[key]
        for key, value in pais.items():
            if key != "codigo":
                suma = suma + float(value)         
    promedio = suma / total
    print(f"El promedio de toneladas de CO2 emitidas por todos los paı́ses es de {promedio} toneladas y su suma total es de {suma} toneladas.")


def total_paises(data):
    total = len(data)

    return total

def main():
    dict = openfile()
    calcula_promedio_total(dict)

main()