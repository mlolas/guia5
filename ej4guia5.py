filename = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():
    archivo = open(filename)
    dict = {}

    for counter, linea in enumerate(archivo):
        if counter != 0:
            archivo_linea = linea.split(",")
            pais = archivo_linea[PAIS].strip()
            codigo = archivo_linea[CODIGO].strip()
            anio = archivo_linea[ANIO].strip()
            co2 = archivo_linea[CO2].strip()

            if pais == "World":
                continue

            temp_dic = {"codigo": codigo,
                            anio: co2}

            if dict.get(pais):
                dict[pais].update(temp_dic)
            else:
                dict[pais] = temp_dic
    archivo.close()

    return dict

def cant_muestras_co2(data):
    mayor_co2 = 0
    for key, value in data.items():
        pais = data[key]
        pais_mayor_co2 = key
        counter = 0
        sumatoria = 0
        for key, value in pais.items():
            if key != "codigo":
                counter = counter + 1
            if key != "codigo":
                sumatoria = sumatoria + float(value)
        promedio = sumatoria / counter
        if promedio > mayor_co2:
            mayor_co2 = promedio
            pais_cant_mayor = pais_mayor_co2                
    print(f"El país que emitió la mayor cantidad de toneladas de CO2 es {pais_cant_mayor} con {mayor_co2} toneladas en promedio.")

def main():
    dict = openfile()
    cant_muestras_co2(data=dict)

main()