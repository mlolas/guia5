filename = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():
    archivo = open(filename)
    dict = {}

    for counter, linea in enumerate(archivo):
        if counter != 0:
            archivo_linea = linea.split(",")
            pais = archivo_linea[PAIS].strip()
            codigo = archivo_linea[CODIGO].strip()
            anio = archivo_linea[ANIO].strip()
            co2 = archivo_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                            anio: co2}

            if dict.get(pais):
                dict[pais].update(temp_dic)
            else:
                dict[pais] = temp_dic
    archivo.close()

    return dict

def consultar_pais():
    pais = input("Ingrese el nombre del país para ver sus emisiones de CO2: ")
    print("\n")
    return pais.capitalize()

def imprime_data(data, pais):
    dict = data[pais]
    for key, value in dict.items():
        print(key, value)

def sumatoria(data, pais):
    dict = data[pais]
    suma = 0
    for key, value in dict.items():
        if key != "codigo":
            suma = suma + float(value)
    print(f"\nEl total de CO2 emitido por {pais} es de {suma} toneladas.")

def promedio(data, pais):
    dict = data[pais]
    counter = 0
    suma = 0
    for key, value in dict.items():
        if key != "codigo":
            counter = counter + 1
        if key != "codigo":
            suma = suma + float(value)
    promedio = suma / counter
    print(f"\nEl promedio de CO2 emitido por {pais} es de {promedio} toneladas.\n")

def main():
    dict = openfile()
    pais = consultar_pais()
    imprime_data(data=dict, pais=pais)
    sumatoria(data=dict, pais=pais)
    promedio(data=dict, pais=pais)

main()