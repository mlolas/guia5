filename = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():
    archivo = open(filename)
    dict = {}

    for counter, linea in enumerate(archivo):
        if counter != 0:
            archivo_linea = linea.split(",")
            pais = archivo_linea[PAIS].strip()
            codigo = archivo_linea[CODIGO].strip()
            anio = archivo_linea[ANIO].strip()
            co2 = archivo_linea[CO2].strip()

            temp_dic = {"codigo": codigo,
                            anio: co2}

            if dict.get(pais):
                dict[pais].update(temp_dic)
            else:
                dict[pais] = temp_dic
    archivo.close()

    return dict

def cant_muestras_co2(data):
    menor_co2 = 267
    counter = 0
    for key, value in data.items():
        pais = data[key]
        pais_menor_co2 = key
        counter = 0
        for key, value in pais.items():
            if key != "codigo":
                counter = counter + 1
        if counter < menor_co2:
            menor_co2 = counter
            pais_cant_menor = pais_menor_co2
    print(f"El país con el menor número de muestras o registros de estudio es {pais_cant_menor} con {menor_co2} muestras.")

def main():
    dict = openfile()
    cant_muestras_co2(data=dict)

main()