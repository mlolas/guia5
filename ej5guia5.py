filename = "co2_emission.csv"

(PAIS,
 CODIGO,
 ANIO,
 CO2) = range(4)

def openfile():
    archivo = open(filename)
    dict = {}

    for counter, linea in enumerate(archivo):
        if counter != 0:
            archivo_linea = linea.split(",")
            pais = archivo_linea[PAIS].strip()
            codigo = archivo_linea[CODIGO].strip()
            anio = archivo_linea[ANIO].strip()
            co2 = archivo_linea[CO2].strip()

            if pais == "Statistical differences":
                continue

            temp_dic = {"codigo": codigo,
                            anio: co2}

            if dict.get(pais):
                dict[pais].update(temp_dic)
            else:
                dict[pais] = temp_dic
    archivo.close()

    return dict

def cant_muestras_co2(data):
    menor_co2 = 1704900066.8898306
    for key, value in data.items():
        pais = data[key]
        pais_menor_co2 = key
        sumatoria = 0
        counter = 0
        for key, value in pais.items():
            if key != "codigo":
                counter = counter + 1
            if key != "codigo":
                sumatoria = sumatoria + float(value)
        promedio = sumatoria / counter
        if promedio < menor_co2:
            menor_co2 = promedio
            pais_cant_menor = pais_menor_co2                
    print(f"El país que emitió la menor cantidad de toneladas de CO2 es {pais_cant_menor} con {menor_co2} toneladas en promedio.")

def main():
    dict = openfile()
    cant_muestras_co2(data=dict)

main()